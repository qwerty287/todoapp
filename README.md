# todoapp

Very simple todo application that runs in your web browser. It does not store data on a server, but directly on your device using the browser's IndexedDB interface (thus your todos will be deleted once you clear your browser data). If IndexedDB is not available, it will fall back to LocalStorage.

This is a very lightweight (no dependencies) static web app, you can view a running example at <https://qwerty287.codeberg.page/todoapp>. To run your own, just clone this repository and serve it.
