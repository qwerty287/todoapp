const list = document.getElementById("todo-list");
const counter = document.getElementById("counter");
const clearCompleted = document.getElementById("clear-completed");
const clearCompletedMenu = document.getElementById("clear-completed-menu");
const clearAll = document.getElementById("clear-all");
const exportData = document.getElementById("export");
const importData = document.getElementById("import");
const filterAll = document.getElementById("filter-all");
const filterActive = document.getElementById("filter-active");
const filterComplete = document.getElementById("filter-complete");
const newTodo = document.getElementById("new-todo");

let filterState = "";
let todoElems = [];

let isAddingViaImport = false;

function changeEditorMode(id) {
  const editInput = document.getElementById("edit-" + id);
  const labelInput = document.getElementById("title-" + id);

  if (editInput.classList.contains("hidden")) {
    editInput.classList.remove("hidden");
    labelInput.classList.add("hidden");

    let listener = function (evt) {
      if (evt.code === "Enter" && editInput.value.trim()) {
        store.get(id).then((t) => {
          t.title = editInput.value.trim();
          store.put(t).then(loadTodoList, handleErrorDefault);
        }, handleErrorDefault);
      } else if (evt.code === "Escape") {
        editInput.removeEventListener("keyup", listener);
        changeEditorMode(id);
      }
    };

    editInput.addEventListener("keyup", listener);
    editInput.focus();
  } else {
    editInput.classList.add("hidden");
    labelInput.classList.remove("hidden");
  }
}

const handleErrorDefault = function (error) {
  toast(error);
};

function updateTodoState(id) {
  store.get(id).then((t) => {
    t.complete = !t.complete;
    store.put(t).then(loadTodoList, handleErrorDefault);
  }, handleErrorDefault);
}

function deleteTodo(id) {
  store.delete(id).then(loadTodoList, handleErrorDefault);
}

function loadTodoList() {
  store.getAll(filterState).then((todos) => {
    let allComplete = false;
    let anyComplete = false;

    todos = todos.filter(
      (todo) =>
        filterState === "" ||
        (filterState === "completed" && todo.complete) ||
        (filterState === "active" && !todo.complete)
    );
    counter.innerText = todos
      .filter((todo) => !todo.complete)
      .length.toString();

    for (const e of todoElems) {
      e.remove();
    }
    todoElems = [];

    for (const todo of todos) {
      const todoElem = document.createElement("li");
      if (todo.complete) {
        todoElem.className = "completed";
      }
      todoElem.innerHTML = `<div class="view">
                <input onchange="updateTodoState('${
                  todo.id
                }')" type="checkbox" class="toggle"${
        todo.complete ? "checked" : ""
      }>
                
                    <span class="title" id="title-${
                      todo.id
                    }" onclick="changeEditorMode('${todo.id}')">${
        todo.title
      }</span>
                    <input class="hidden edit" id="edit-${todo.id}" value="${
        todo.title
      }" placeholder="What needs to be done?" name="todo-title" autofocus>
                <button onclick="deleteTodo('${
                  todo.id
                }')" class="destroy"></button>
            </div>`;

      todoElems.push(todoElem);
      list.appendChild(todoElem);

      allComplete = allComplete && todo.complete;
      anyComplete = anyComplete || todo.complete;
    }

    if (!anyComplete) {
      clearCompleted.classList.add("hidden");
      clearCompletedMenu.classList.add("hidden");
    } else {
      clearCompleted.classList.remove("hidden");
      clearCompletedMenu.classList.remove("hidden");
    }
    if (!todos.length) {
      clearAll.classList.add("hidden");
    } else {
      clearAll.classList.remove("hidden");
    }
  }, handleErrorDefault);
}

function updateFilters(newState = filterState) {
  filterState = newState;
  if (filterState === "") {
    filterAll.className = "selected";
    filterActive.className = "";
    filterComplete.className = "";
  } else if (filterState === "active") {
    filterAll.className = "";
    filterActive.className = "selected";
    filterComplete.className = "";
  } else if (filterState === "completed") {
    filterAll.className = "";
    filterActive.className = "";
    filterComplete.className = "selected";
  }

  loadTodoList();
}

function addTodo(title) {
  store
    .add({
      title: title,
      complete: false,
    })
    .then(loadTodoList, handleErrorDefault);
}

function addTodoBtn() {
  if (newTodo.value.trim()) {
    if (!isAddingViaImport) {
      addTodo(newTodo.value.trim());
    } else {
      importJson(newTodo.value.trim());
    }
    newTodo.value = "";
    isAddingViaImport = false;
    newTodo.placeholder = "What needs to be done?";
  }
}

function toggleMenu() {
  document.getElementsByTagName("nav")[0].classList.toggle("hidden");
  document.getElementById("nav-background").classList.toggle("hidden");
}

function importJson(json) {
  const todos = JSON.parse(json);
  const add = function (index) {
    if (index >= todos.length) {
      loadTodoList();
      return;
    }

    store.add(todos[index]).then(() => add(index + 1), handleErrorDefault);
  };

  add(0);
}

store.init(loadTodoList, (e) => {
  handleErrorDefault(e);
  document.getElementById("header").className = "hidden";
  document.getElementById("main").className = "hidden";
  document.getElementById("action-bar").className = "hidden";
});

newTodo.addEventListener("keyup", (evt) => {
  if (evt.code === "Enter") {
    addTodoBtn();
  }
});

clearCompleted.addEventListener("click", () => {
  store.deleteCompleted().then(loadTodoList, handleErrorDefault);
});

clearCompletedMenu.addEventListener("click", () => {
  store.deleteCompleted().then(loadTodoList, handleErrorDefault);
  toggleMenu();
});

exportData.addEventListener("click", () => {
  store.getAll().then((todos) => {
    navigator.clipboard.writeText(JSON.stringify(todos));
    toast("Data copied to clipboard successfully", "green");
  }, handleErrorDefault);
  toggleMenu();
});

importData.addEventListener("click", () => {
  newTodo.placeholder = "Paste exported data here";
  isAddingViaImport = true;
  toggleMenu();
});

clearAll.addEventListener("click", () => {
  store.deleteAll().then(loadTodoList, handleErrorDefault);
  toggleMenu();
});

document.addEventListener("keyup", (evt) => {
  if (evt.code === "Escape") {
    document.getElementsByTagName("nav")[0].classList.add("hidden");
    document.getElementById("nav-background").classList.add("hidden");
  }
});
