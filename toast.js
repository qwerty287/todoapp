let _toastsContainer = null;

function toast(message, background = "red") {
  _toastsContainer = document.getElementById("toast");

  let hovered = false;

  const toastDiv = document.createElement("div");
  toastDiv.style["background-color"] = background;
  toastDiv.style["opacity"] = "0";
  toastDiv.addEventListener("mouseenter", () => {
    hovered = true;
  });
  toastDiv.addEventListener("mousemove", () => {
    hovered = true;
  });
  toastDiv.addEventListener("mouseleave", () => {
    hovered = false;
  });
  toastDiv.className = "toast-div";
  toastDiv.innerHTML = message;
  _toastsContainer.appendChild(toastDiv);

  const animationSteps = 20;
  let stepsDone = 0;
  let timer = setInterval(() => {
    if (stepsDone >= animationSteps) {
      clearInterval(timer);
      return;
    }
    stepsDone++;
    toastDiv.style["opacity"] = (stepsDone / animationSteps).toString();
  }, 10);

  setTimeout(() => {
    const hide = () => {
      const animationSteps = 20;
      let stepsDone = 0;
      let timer = setInterval(() => {
        if (stepsDone >= animationSteps) {
          toastDiv.remove();
          clearInterval(timer);
          return;
        }
        stepsDone++;
        toastDiv.style["opacity"] = (1 - stepsDone / animationSteps).toString();
      }, 10);
    };
    if (hovered) {
      toastDiv.addEventListener("mouseleave", hide);
    } else {
      hide();
    }
  }, 3000);
}
