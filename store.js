function randomString(length) {
  let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  let str = "";
  for (let i = 0; i < length; i++) {
    str += chars.charAt(Math.floor(Math.random() * chars.length));
  }

  return str;
}

const store = {
  _store: null,

  init: function (resolve, reject) {
    store._store = _idb;
    store._store.init().then(resolve, () => {
      // if there's an error, we fall back to LocalStorage
      store._store = _ls;
      store._store.init().then(resolve, reject);
    });
  },

  add: function (todo) {
    todo.id = randomString(16);
    return store._store.add(todo);
  },

  getAll: function (filter = "") {
    return store._store.getAll(filter);
  },

  get: function (id) {
    return store._store.get(id);
  },

  put: function (todo) {
    return store._store.put(todo);
  },

  delete: function (id) {
    return store._store.delete(id);
  },

  deleteCompleted: function () {
    return store._store.deleteCompleted();
  },

  deleteAll: function () {
    return store._store.deleteAll();
  },
};

const _idb = {
  db: null,
  dbVersion: 1,
  transaction: (requiresWrite = false) => {
    return _idb.db
      .transaction("todos", requiresWrite ? "readwrite" : "readonly")
      .objectStore("todos");
  },

  init: function () {
    return new Promise((resolve, reject) => {
      if (!window.indexedDB) {
        reject("Your browser does not support IndexedDB.");
      }

      const idb = indexedDB.open("todo", _idb.dbVersion);

      idb.onupgradeneeded = function (evt) {
        // the existing database version is less than 2 (or it doesn't exist)
        let db = evt.target.result;
        switch (evt.oldVersion) {
          case 0:
            db.createObjectStore("todos", { keyPath: "id" });
        }
      };

      idb.onerror = function () {
        reject(idb.error);
      };

      idb.onsuccess = function (evt) {
        _idb.db = evt.target.result;
        resolve();
      };
    });
  },

  add: function (todo) {
    return new Promise((resolve, reject) => {
      const r = _idb.transaction(true).add(todo);

      r.onerror = function (evt) {
        reject(evt.target.errorCode);
      };

      r.onsuccess = function () {
        resolve();
      };
    });
  },

  getAll: function (filter = "") {
    return new Promise((resolve, reject) => {
      const a = _idb.transaction().openCursor();

      a.onerror = function (evt) {
        reject(evt.target.errorCode);
      };

      const all = [];

      a.onsuccess = function (evt) {
        const todoCursor = evt.target.result;
        if (todoCursor) {
          const todo = todoCursor.value;
          if (
            filter === "" ||
            (filter === "completed" && todo.complete) ||
            (filter === "active" && !todo.complete)
          ) {
            all.push(todo);
          }
          todoCursor.continue();
        } else {
          resolve(all);
        }
      };
    });
  },

  get: function (id) {
    return new Promise((resolve, reject) => {
      const a = _idb.transaction().get(id);

      a.onerror = function (evt) {
        reject(evt.target.errorCode);
      };

      a.onsuccess = function (evt) {
        resolve(evt.target.result);
      };
    });
  },

  put: function (todo) {
    return new Promise((resolve, reject) => {
      const a = _idb.transaction(true).put(todo);

      a.onerror = function (evt) {
        reject(evt.target.errorCode);
      };

      a.onsuccess = function (evt) {
        resolve(evt.target.result);
      };
    });
  },

  delete: function (id) {
    return new Promise((resolve, reject) => {
      const a = _idb.transaction(true).delete(id);

      a.onerror = function (evt) {
        reject(evt.target.errorCode);
      };

      a.onsuccess = function (evt) {
        resolve(evt.target.result);
      };
    });
  },

  deleteCompleted: function () {
    return _idb.deleteFromFilter("completed");
  },

  deleteAll: function () {
    return _idb.deleteFromFilter("");
  },

  deleteFromFilter: function (filter) {
    return new Promise((resolve, reject) => {
      _idb.getAll(filter).then((todos) => {
        const os = _idb.transaction(true);
        const runDelete = function (index) {
          if (index >= todos.length) {
            resolve();
            return;
          }

          const a = os.delete(todos[index].id);

          a.onerror = function (evt) {
            reject(evt.target.errorCode);
          };

          a.onsuccess = function () {
            runDelete(index + 1);
          };
        };

        runDelete(0);
      }, reject);
    });
  },
};

const _ls = {
  init: function () {
    return new Promise((resolve) => {
      resolve();
    });
  },

  add: function (todo) {
    return new Promise((resolve) => {
      localStorage.setItem(todo.id, JSON.stringify(todo));
      resolve();
    });
  },

  getAll: function (filter = "") {
    return new Promise((resolve) => {
      const all = [];
      for (let i = 0; i < localStorage.length; i++) {
        const todo = JSON.parse(localStorage.getItem(localStorage.key(i)));
        if (
          filter === "" ||
          (filter === "completed" && todo.complete) ||
          (filter === "active" && !todo.complete)
        ) {
          all.push(todo);
        }
      }
      resolve(all);
    });
  },

  get: function (id) {
    return new Promise((resolve) => {
      resolve(JSON.parse(localStorage.getItem(id)));
    });
  },

  put: function (todo) {
    return _ls.add(todo);
  },

  delete: function (id) {
    return new Promise((resolve) => {
      localStorage.removeItem(id);
      resolve();
    });
  },

  deleteCompleted: function () {
    return _ls.deleteFromFilter("completed");
  },

  deleteAll: function () {
    return _ls.deleteFromFilter("");
  },

  deleteFromFilter: function (filter) {
    return new Promise((resolve, reject) => {
      _ls.getAll(filter).then((todos) => {
        const runDelete = function (index) {
          if (index >= todos.length) {
            resolve();
            return;
          }

          localStorage.removeItem(todos[index].id);
          runDelete(index + 1);
        };

        runDelete(0);
      }, reject);
    });
  },
};
